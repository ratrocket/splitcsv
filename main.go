// Split a CSV file, preserving header in each file.
//
// Notes
// - files are created in the current dir (could have option to create
//   elsewhere)
// - names of split files are based on name of original file (could have
//   option to have it differ)
// - when you say you want N files, you might get N+1 files
// - if you say nlines = N, each file will have N+1 lines (the header
//   line)
//
// Other possible options:
// option to keep header or not
// option to say whether there *is* a header or not
//
// Maybe the file option should just be the last thing on the command
// line?
//
// Consider looking at the split utility (`man split`) and think about
// making *this* more like *that*.
package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strings"
)

func digitsIn(i int) int {
	if i < 100 {
		return 2
	}
	d := 0
	for ; i > 0; d++ {
		i /= 10
	}
	return d
}

func main() {
	var (
		file   = flag.String("f", "", "file to split")
		nlines = flag.Int("l", 0, "(maximum) lines per split file")
		nfiles = flag.Int("n", 0, "number of files desired")
		lpf    int // lines (of content, not header!) per file; the "step" of the for loop
		nf     int // (actual) number of files to produce
	)
	flag.Parse()
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Split file\n")
		flag.PrintDefaults()
		os.Exit(1)
	}
	if *file == "" {
		fmt.Fprintf(os.Stderr, "filename required\n")
		flag.Usage()
	}
	if (*nlines > 0 && *nfiles > 0) || (*nlines == 0 && *nfiles == 0) {
		fmt.Fprintf(os.Stderr, "provide only nlines or nfiles (-l or -n; XOR)\n")
		flag.Usage()
	}

	data, err := ioutil.ReadFile(*file)
	if err != nil {
		log.Fatal(err)
	}
	lines := bytes.Split(data, []byte("\n"))
	if bytes.Equal(lines[len(lines)-1], []byte("")) {
		lines = lines[:len(lines)-1]
	}
	header := lines[0]
	lines = lines[1:]

	lpf = *nlines
	if lpf == 0 {
		lpf = len(lines) / *nfiles
	}

	// Only need nf to know how wide the number in the split files's
	// name needs to be.
	nf = len(lines) / lpf
	if len(lines)%lpf != 0 {
		nf += 1
	}

	base := path.Base(*file)
	ext := path.Ext(base)
	bare := strings.TrimSuffix(base, ext)
	namer := func(nfiles int) func(int) string {
		// Format string for returned func, "width" of the digit
		// in the eventual filename tuned to the number of files
		// being created.
		wf := fmt.Sprintf("%%s-%%0%dd%%s", digitsIn(nfiles))
		return func(i int) string {
			return fmt.Sprintf(wf, bare, i, ext)
		}
	}
	name := namer(nf)

	left := 0
	i := 1
	var b [][]byte
	for left < len(lines) {
		right := left + lpf
		if right > len(lines) {
			right = len(lines)
		}
		b = append([][]byte{header}, lines[left:right]...)
		// append an "extra" "\n" so the file ends in a newline
		contents := append(bytes.Join(b, []byte("\n")), []byte("\n")...)
		err := ioutil.WriteFile(name(i), contents, 0644)
		if err != nil {
			log.Fatal(err)
		}
		left += lpf
		i++
	}
}
