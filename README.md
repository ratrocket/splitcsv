# splitcsv

> **(May 2021)** moved to [md0.org/splitcsv](https://md0.org/splitcsv).

Split CSV files into multiple CSV files, preserving the header.

## "Testing"

Given a file "cats.csv":

### Check output with diff

diff <(sort -u cats.csv) <(cat cats-*.csv | sort -u)

(should be no output)

### Check by counting lines

- given you end up with N files,
- given wc -l cats.csv   = X,
- given wc -l cats-*.csv = Y,
- then, X-1 + N = Y

X-1 is the lines in cats.csv minus the header line.

"+ N" accounts for the header line in each of the N files.
